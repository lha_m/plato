package com.elham.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username , password;
    Button signIn , signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        signUp = findViewById(R.id.createAccount);
        signIn = findViewById(R.id.signin);
        signUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent intent=new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }
}
