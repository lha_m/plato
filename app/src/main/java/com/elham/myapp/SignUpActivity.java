package com.elham.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username , password;
    Button signUp;
    TextView invalidPass , invalidUsername;
    boolean validUsername , validPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        username = findViewById(R.id.newUsername);
        password = findViewById(R.id.newPassword);
        signUp = findViewById(R.id.signUp);
        invalidPass = findViewById(R.id.invalidPass);
        invalidUsername = findViewById(R.id.invalidUsername);

        invalidPass.setVisibility(View.INVISIBLE);

        invalidUsername.setVisibility(View.INVISIBLE);

        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String useName = username.getText().toString();
                if( TextUtils.isEmpty(useName)){
                    invalidUsername.setVisibility(View.VISIBLE);
                }else{
                    invalidUsername.setVisibility(View.INVISIBLE);
                    validUsername=true;
                }
            }
        });


        signUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String pass = password.getText().toString();
        if(pass.length()<5){
            invalidPass.setVisibility(View.VISIBLE);
        }else{
            invalidPass.setVisibility(View.INVISIBLE);
            validPass=true;
            if(validUsername){

            }
        }

    }


}
